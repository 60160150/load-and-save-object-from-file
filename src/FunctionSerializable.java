
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gor yaa
 */
public class FunctionSerializable {
    
    public static boolean writeUserToFlie(User user ,String filename){
        boolean status= true;
        FileOutputStream fos =null;
        ObjectOutputStream oos =null;
        try {
            fos = new FileOutputStream(filename);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(user);
            
        } catch (FileNotFoundException ex) {
            status= false;
        } catch (IOException ex) {
            status= false;
        }
        
        if (oos != null){
            try {
                oos.close();
            } catch (IOException ex) {
                status= false;
            }
        }
        if (fos != null){
            try {
                fos.close();
            } catch (IOException ex) {
                status= false;
            }
        }
        return status;
    }
    
    public static void main(String[] args){
        User Goryaa = new User ("Utsadawut","Sangthong","Goryaa","Goryaa08523","089999999",61.0f,170f);
        System.out.println("Goryaa");
        writeUserToFlie(Goryaa,"Goryaa.bin");
        
        
   
    }
    
}
