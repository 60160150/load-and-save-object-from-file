
import java.io.Serializable;



/**
 *
 * @author Gor yaa
 */
public class User implements Serializable {

    private String name;
    private String surname;
    private String password;
    private String username;
    private String tel;
    private float weight;
    private float height;

    public User(String name, String surname, String password, String username, String tel, float weight, float height) {
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.username = username;
        this.tel = tel;
        this.weight = weight;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", surname=" + surname + ", password=" + password + ", username=" + username + ", tel=" + tel + ", weight=" + weight + ", height=" + height + '}';
    }
    
    
    
}
