
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 *
 * @author Gor yaa
 */
public class FunctionReadObject {
    public static User readUserFromFile(String fliename){
        FileInputStream fis =null;
        ObjectInputStream ois =null;
        User user = null;
        
        
        
        try {
            fis = new FileInputStream(fliename);
            ois = new ObjectInputStream(fis);
            user = (User) ois.readObject();
        } catch (FileNotFoundException ex) {
            user = null;
        } catch (IOException ex) {
           user = null;
        } catch (ClassNotFoundException ex) {
            user = null;
        }
        
        if(fis != null){
            try {
                fis.close();
            } catch (IOException ex) {
                user = null;
            }
        }
        return user;
    }
    public static void main(String[] args){
        System.out.println(readUserFromFile("Goryaa.bin"));
    }
    
}
