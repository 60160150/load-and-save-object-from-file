
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gor yaa
 */
public class UserList {
    public static boolean writeUserListToFlie(ArrayList<User> userList ,String filename){
        boolean status= true;
        FileOutputStream fos =null;
        ObjectOutputStream oos =null;
        try {
            fos = new FileOutputStream(filename);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            
        } catch (FileNotFoundException ex) {
            status= false;
        } catch (IOException ex) {
            status= false;
        }
        
        if (oos != null){
            try {
                oos.close();
            } catch (IOException ex) {
                status= false;
            }
        }
        if (fos != null){
            try {
                fos.close();
            } catch (IOException ex) {
                status= false;
            }
        }
        return status;
    }
    
    public static void main(String[] args){
        ArrayList<User> userList =new ArrayList();
        User Goryaa = new User ("Utsadawut","Sangthong","Goryaa","Goryaa08523","089999999",61.0f,170f);
        User Bew = new User ("Utsadawut","Sangthong","Goryaa","Goryaa08523","089999999",61.0f,170f);
        userList.add(Goryaa);
        userList.add(Bew);
        
        writeUserListToFlie(userList,"contact.bin");
    }
    
}
