
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gor yaa
 */
public class TestSerializable {
    public static void main(String[] args){
        User Goryaa = new User ("Utsadawut","Sangthong","Goryaa","Goryaa08523","089999999",61.0f,170f);
        System.out.println("Goryaa");
        
        FileOutputStream fos =null;
        ObjectOutputStream oos =null;
        try {
            fos = new FileOutputStream("Goryaa");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(Goryaa);
            
        } catch (FileNotFoundException ex) {
            System.out.println("Error");
        } catch (IOException ex) {
            System.out.println("Error");
        }
        
        if (oos != null){
            try {
                oos.close();
            } catch (IOException ex) {
                System.out.println("Error");
            }
        }
        if (fos != null){
            try {
                fos.close();
            } catch (IOException ex) {
                System.out.println("Error");
            }
        }
    }
    
}
