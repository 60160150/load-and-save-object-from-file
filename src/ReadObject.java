
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 *
 * @author Gor yaa
 */
public class ReadObject {
    public static void main(String[] args){
        FileInputStream fis =null;
        ObjectInputStream ois =null;
        User Goryaa = null;
        
        
        
        try {
            fis = new FileInputStream("Goryaa.bin");
            ois = new ObjectInputStream(fis);
            Goryaa = (User) ois.readObject();
            System.out.println(Goryaa);
        } catch (FileNotFoundException ex) {
            System.out.println("Error");
        } catch (IOException ex) {
            System.out.println("Error");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error");
        }
        
        if(fis != null){
            try {
                fis.close();
            } catch (IOException ex) {
                System.out.println("Error");
            }
        }
    }
    
}
